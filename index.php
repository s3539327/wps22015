<html>	
<head>
	<title>Silverado Cinemas</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="CSS/style.css">
	<link rel="stylesheet" type="text/css" href="CSS/mediaqueries.css">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Crimson+Text" />
	<link href='https://fonts.googleapis.com/css?family=Miltonian' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="Scripts/jscript.js"></script>
	<script type="text/javascript" src="Scripts/navbar.js"></script>	

	</style>
</head>
<body>

<div id="topofPage" id="wrapper" class="Container";>
	<div class="LogoSpace">
		<img src="Images/logo.png" alt="Silverado's Logo">
	</div>
	<div id="Container">
		<div id="NavBar">
			<ul class="clear-after">
			<li><a href="index.php"><img class="navButton1" src="Buttons/home.png" alt="home" style="width:150px; max-width: 100%;"></a></li>
        	<li><a href="movies.html"><img class="navButton2" src="Buttons/movies.png" alt="movies" style="width:150px; max-width: 100%;"></a></li>
        	<li><a href="bookings.html"><img class="navButton3" src="Buttons/bookings.png" alt="bookings" style="width:150px; max-width: 100%;"></a></li>
        	<li><a href="contact.html"><img class="navButton4" src="Buttons/contact.png" alt="contact" style="width:150px; max-width: 100%;"></a></li>
		</div>
	<br>
	
	<div class="tiles clear-before full">
		<h1>GET READY FOR SILVERADO'S GRAND RE-OPENING!!!</h1>
	</div>
			
	<div class="tiles full">		
		<h2>The Silverado Cinemas you've always known has gotten a major upgrade</h2>
			<p> The cinema that's been serving locals their favourite flicks 
			for years is now even more accessible with new seats, new sound 
			and a 3D projector that'll have you fully immersed in whichever 
			film you choose to watch!
	</div>
	
	<br>
	
	<div class="homeImages" id="real3DLogo">
		<img src="Images/3DLogo.png" id="real3DLogo"/>
	</div>
	
	<div class="tiles textBox">
		<h3> Experience it in 3D </h3>
			<p>Silverado's been a bit behind the times, and we admit it. We've watched as our beloved townsfolk
			and loyal customers left town to undertake a pilgrimage to the next town over or to wherever it maybe
			that has a modern cinema that can immerse them in their jaw-dropping, heart-racing, adrenaline-rushing
			3D projections. And we got the message. We were outdated. That's why we, Silverado Cinemas, have gotten
			ourselves a brand new 3D Projector! <br>
			<p>Prepare to have yourself blown away by the 3D experience right here
			without having to pay for the mileage that you'll probably spend travelling to the next town over. Use
			that fuel money to get yourself some popcorn and a drink because you can watch that new movie in beautiful
			3D right here. At home. At Silverado Cinemas.
	</div>		
	
	<br>
	
	<div class="tiles full clear-after">
		<div>
			<h2> NEW Seats </h2>
				<p> Now with bean bags and first class seating, to give you more
				choice about how you view your choicest films.
		</div>
	</div>
	
	<br>
	
	<div class="homeImages" >
		<img src="Images/NormalSeats.jpg" id="normalSeats"/>
	</div>
	
	<div class="tiles textBox">
		<h3> UPGRADED Normal Viewing </h3>
			<br>
			<p>The renovation brings with it some new and even better seating 
			options. There are now 40 new seats for general viewing that ensure 
			absolute comfort for whatever duration of time your movie is. These seats span some 
			of the best positions in the cinema, to ensure that you get the best 
			movie experience you can get. These seats accommodate the whole family, 
			with child and adult pricing options.
	</div>
	
	<br>
	
	<div class="homeImages">
		<img src="Images/FirstClassSeats.jpg"  id="firstClass"/>
	</div>
	
	<div class="tiles textBox">
		<h3> NEW First Class </h3>
			<br>
			<p>For those who want the best possible movie-going experience,
			they can expect to get just that when they sit in one of the 12
			brand new First Class seats. These seats offer the best positioning
			in the cinema ensuring the best viewing angle, the best auditory
			experience and the most comfort you could possibly experience in a
			cinema. These seats accommodate the whole family, with child and 
			adult pricing.
	</div>

	<div class="tiles full">
		<h3> NEW Bean Bags - Whole Family Fun!</h3>
			<p>For the children who can't sit still for a more than a minute,
			or for the older viewers who want a different cinema going experience, 
			Silverado provides the perfect alternative to cinema seats; Bean Bags! 
			Silverado ofrs something the bigs chains could never hope to achieve; a 
			great cinema experience and the comfort of bean bags. With each bean bag 
			accomodating 2 adults, 1 adult and 1 child or 1 adult and 3 children under 
			12, there's enough bean-baggy goodness for everyone. And better yet, it's 
			one price fits all!
	</div>
	
	<br>
	
	<div class="homeImages">
		<img src="Images/DolbyAtmos.jpg"  id="dolbyAtmos"/>	
	</div>
	
	<div class="tiles textBox">
		<h3> NEW DOLBY ATMOS CINEMA SOUND</h3>
			<br>
			<p>Silverado has gone all out for its loyal customers by installing a brand
			new sound system that even major chain cinemas are using! Dolby's Atmos ensures
			a crystal clear, enchanting experience, with every sound being exactly that – a beautifully
			crisp and sharp audio experience. Be it rainforest ambience, the sound of cars racing down a
			gravel track or an action sequence with explosions and gunfire, with Dolby Atmos, you'll be
			transported right to where the action is to experience every sound and detail you could
			possibly hear, all in crystal clear Dolby HD audio.
	</div>
	
	<br>
	
	<div class="homeImages full clear-before" id="dolbyVisionBox">
		<img id="dolbyVision" src="Images/DolbyVision.jpg"/>
	</div>
	
	<div class="tiles full clear-after">
		<h3> NEW DOLBY VISION CINEMA DISPLAY</h3>
			<br>
			<p>They say there is nothing like the going to the cinema, and they say that for a
			reason. Nothing compares to the experience of the big screen and vibrant colours you can
			get on those large cinema screens. And now Silverado has made it even better by installing
			Dolby Vision and a 3D projector! Dolby Vision offers a better visual experience, with even
			more vibrant, larger colour palettes and higher contrast. Colour will look more colourful and
			blacks will be just that – black. With more color, more contrast and a screen that stretches
			from wall to wall, see the movie the way it was meant to be seen:
			<h4> With Dolby Vision at Silverado Cinema</h4>
	</div>
</div>

<br>
<br>
	<div style="font-size: 10pt; color: grey">By Sotoam Bak s3542733 and Andrew Johnson s3539327
	</div><br><br>

<div id="topBar">
	<br>
	<a href="#topofPage">To top of Page</a>
	<br>
</div>

</body>
</html>