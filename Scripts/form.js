function hide(id){
	if (document.getElementById && document.createTextNode){
		spans=document.getElementById('bookings').getElementsByTagName('span');
		m=document.getElementById('bookings');
		for (i=0;i<spans.length;i++){
			m.getElementsByTagName('span')[i].style.visibility='hidden';
		}
		if(id!='none'){			
			m.getElementsByTagName('span')[id].style.visibility='visible';
		}	
	}
}

function totalPrice(){
	var totalPrice = saPrice() + spPrice() + scPrice() + faPrice() + fcPrice() + b1Price() + b2Price() + b3Price();
	var theForm = document.forms["bookings"];
	document.getElementById('totalDisplay').innerHTML = "$"+totalPrice+".00";
	document.getElementById('price').value = 'totalPrice';
	return totalPrice;
}

function saPrice(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;
	ticketNumber["05"] = 5;
	ticketNumber["06"] = 6;
	ticketNumber["07"] = 7;
	ticketNumber["08"] = 8;
	ticketNumber["09"] = 9;
	ticketNumber["10"] = 10;


	var saSubtotal = 0;
	var ticketPrice = 12;
	var theForm = document.forms["bookings"];
	var saTickets = theForm.elements["SA"];
	var saTicketNumber = ticketNumber[SA.value];
	saSubtotal = saTicketNumber * ticketPrice;

	document.getElementById('saDisplay').innerHTML = "$"+saSubtotal+".00";
	return saSubtotal;
}

function spPrice(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;
	ticketNumber["05"] = 5;
	ticketNumber["06"] = 6;
	ticketNumber["07"] = 7;
	ticketNumber["08"] = 8;
	ticketNumber["09"] = 9;
	ticketNumber["10"] = 10;


	var spSubtotal = 0;
	var ticketPrice = 10;
	var theForm = document.forms["bookings"];
	var spTickets = theForm.elements["SP"];
	var spTicketNumber = ticketNumber[SP.value];
	spSubtotal = spTicketNumber * ticketPrice;

	document.getElementById('spDisplay').innerHTML = "$"+spSubtotal+".00";
	return spSubtotal;	
}


function scPrice(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;
	ticketNumber["05"] = 5;
	ticketNumber["06"] = 6;
	ticketNumber["07"] = 7;
	ticketNumber["08"] = 8;
	ticketNumber["09"] = 9;
	ticketNumber["10"] = 10;


	var scSubtotal = 0;
	var ticketPrice = 8;
	var theForm = document.forms["bookings"];
	var scTickets = theForm.elements["SC"];
	var scTicketNumber = ticketNumber[SC.value];
	scSubtotal = scTicketNumber * ticketPrice;

	document.getElementById('scDisplay').innerHTML = "$"+scSubtotal+".00";
	return scSubtotal;	
}

function faPrice(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;
	ticketNumber["05"] = 5;


	var faSubtotal = 0;
	var ticketPrice = 25;
	var theForm = document.forms["bookings"];
	var faTickets = theForm.elements["FA"];
	var faTicketNumber = ticketNumber[FA.value];
	faSubtotal = faTicketNumber * ticketPrice;

	document.getElementById('faDisplay').innerHTML = "$"+faSubtotal+".00";
	return faSubtotal;	
}

function fcPrice(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;
	ticketNumber["05"] = 5;

	var fcSubtotal = 0;
	var ticketPrice = 20;
	var theForm = document.forms["bookings"];
	var fcTickets = theForm.elements["FC"];
	var fcTicketNumber = ticketNumber[FC.value];
	fcSubtotal = fcTicketNumber * ticketPrice;

	document.getElementById('fcDisplay').innerHTML = "$"+fcSubtotal+".00";
	return fcSubtotal;	
}

function b1Price(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;



	var b1Subtotal = 0;
	var ticketPrice = 20;
	var theForm = document.forms["bookings"];
	var b1Tickets = theForm.elements["B1"];
	var b1TicketNumber = ticketNumber[B1.value];
	b1Subtotal = b1TicketNumber * ticketPrice;

	document.getElementById('b1Display').innerHTML = "$"+b1Subtotal+".00";
	return b1Subtotal;	
}

function b2Price(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;



	var b2Subtotal = 0;
	var ticketPrice = 20;
	var theForm = document.forms["bookings"];
	var b2Tickets = theForm.elements["B2"];
	var b2TicketNumber = ticketNumber[B2.value];
	b2Subtotal = b2TicketNumber * ticketPrice;

	document.getElementById('b2Display').innerHTML = "$"+b2Subtotal+".00";
	return b2Subtotal;	
}

function b3Price(){
	var ticketNumber = new Array();
	ticketNumber["00"] = 0;
	ticketNumber["01"] = 1;
	ticketNumber["02"] = 2;
	ticketNumber["03"] = 3;
	ticketNumber["04"] = 4;



	var b3Subtotal = 0;
	var ticketPrice = 20;
	var theForm = document.forms["bookings"];
	var b3Tickets = theForm.elements["B3"];
	var b3TicketNumber = ticketNumber[B3.value];
	b3Subtotal = b3TicketNumber * ticketPrice;

	document.getElementById('b3Display').innerHTML = "$"+b3Subtotal+".00";
	return b3Subtotal;	
}