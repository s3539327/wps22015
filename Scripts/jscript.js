function toggle_Div(div_id1, div_id2){
	if(false == $(div_id1).is(':visible')){
		$(div_id1).show();
		$(div_id2).hide();
	}
	else {
		$(div_id1).hide();
		$(div_id2).show();
	}
}